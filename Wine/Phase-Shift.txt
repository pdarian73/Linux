%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Creates isolated 32-bit prefix for Phase Shift

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Installation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Phase Shift
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX='/home/espionage724/Wine Prefixes/Phase Shift' WINEARCH=win32 wine '/home/espionage724/Downloads/ps_release_1.27_full.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% Finish Up
%%%%%%%%%%%%%%%%%%%%

rm '/home/espionage724/Downloads/ps_release_1.27_full.exe' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Launcher
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove Old
%%%%%%%%%%%%%%%%%%%%

rm -R '/home/espionage724/.local/share/applications/wine/Programs/Phase Shift' && mkdir -p '/home/espionage724/.local/share/applications/wine/Programs/Phase Shift'

%%%%%%%%%%%%%%%%%%%%
%%%%% Phase Shift
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/.local/share/applications/wine/Programs/Phase Shift/Phase Shift.desktop'

-------------------------
[Desktop Entry]
Name=Phase Shift
Categories=Game;
Exec=env WINEDEBUG=-all WINEDLLOVERRIDES='msvcr120=n,b' WINEPREFIX='/home/espionage724/Wine Prefixes/Phase Shift' wine '/home/espionage724/Wine Prefixes/Phase Shift/drive_c/Program Files/Phase Shift/phase_shift.exe'
Type=Application
StartupNotify=true
Path=/home/espionage724/Wine Prefixes/Phase Shift/drive_c/Program Files/Phase Shift
Icon=1B74_phase_shift.0
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################