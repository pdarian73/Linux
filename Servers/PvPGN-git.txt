%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- PvPGN setup for Realm of Espionage
- openSUSE Tumbleweed

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Firewall Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/sysconfig/SuSEfirewall2' && sudo systemctl restart 'SuSEfirewall2'

-------------------------
FW_SERVICES_EXT_TCP="6112 6200"
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TODO: Verify if LUA 5.1 is needed or if updated can be used

sudo zypper install distcc gcc gcc-c++ make autoconf cmake git-core zlib-devel libmysqlclient-devel lua51-devel

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Environment Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

TODO: Fix the rest of this line and below

sudo useradd -r -U 'pvpgn' && sudo passwd 'pvpgn' && sudo mkdir -p '/srv/trinity/build' '/srv/trinity/run' && sudo chown -R pvpgn:pvpgn '/srv/pvpgn'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Download Source
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Git Management
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Download
%%%%%%%%%%%%%%%%%%%%

cd '/home/espionage724' && git clone -b master https://github.com/pvpgn/pvpgn-server.git pvpgn && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Update
%%%%%%%%%%%%%%%%%%%%

cd '/home/espionage724/pvpgn' && git pull origin master && cd '/home/espionage724' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Compile and Install PvPGN
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Compile
%%%%%%%%%%%%%%%%%%%%

mkdir '/home/espionage724/build-pvpgn' '/home/espionage724/run-pvpgn' && cd '/home/espionage724/build-pvpgn'
CC='distcc gcc' CXX='distcc g++' cmake '/home/espionage724/pvpgn' -DCMAKE_INSTALL_PREFIX='/home/espionage724/run-pvpgn' -DWITH_MYSQL=1 -DWITH_LUA=1 -DWITH_WIN32_GUI=0 -DCMAKE_CXX_FLAGS='-Ofast -pipe -march=amdfam10' -DCMAKE_C_FLAGS='-Ofast -pipe -march=amdfam10' && sync
DISTCC_HOSTS='192.168.1.150/8 localhost/2' make -j10 && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Install
%%%%%%%%%%%%%%%%%%%%

sudo make install && sudo chown -R espionage724:espionage724 '/home/espionage724/build-pvpgn' '/home/espionage724/run-pvpgn' && cd '/home/espionage724' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configure PvPGN
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Notes
%%%%%%%%%%%%%%%%%%%%

- Tested working with WarCraft III and Diablo II (this includes the firewall rule too)
- This sets up a Diablo II open server; closed needs much more setup and win32-only D2GS

%%%%%%%%%%%%%%%%%%%%
%%%%% bnetd.conf
%%%%%%%%%%%%%%%%%%%%

cp '/home/espionage724/run-pvpgn/etc/pvpgn/bnetd.conf' '/home/espionage724/run-pvpgn/etc/pvpgn/bnetd-real.conf'
nano '/home/espionage724/run-pvpgn/etc/pvpgn/bnetd-real.conf'

-------------------------
storage_path = "sql:mode=mysql;host=192.168.1.154;name=pvpgn;user=pvpgn;pass=X;default=0;prefix=pvpgn_"
kick_old_login = false
hide_addr = true
passfail_count = 5
sync_on_logoff = true
track = 0
servername = "RoE (PvPGN)"
servaddrs = "0.0.0.0:6112"
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Database Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Create Database, pvpgn user, and grant permissions
%%%%% Do on MySQL computer
%%%%%%%%%%%%%%%%%%%%

mysql -u root -p

CREATE DATABASE pvpgn;

CREATE USER pvpgn;
SET PASSWORD FOR 'pvpgn' = PASSWORD ('X');

GRANT ALL PRIVILEGES ON pvpgn.* to 'pvpgn'@'192.168.1.152' IDENTIFIED BY 'X';

FLUSH PRIVILEGES;
EXIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Script Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% bnetd
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/pvpgn.sh' && chmod +x '/home/espionage724/pvpgn.sh'

-------------------------
cd '/home/espionage724/run-pvpgn'
screen -dmS pvpgn '/home/espionage724/run-pvpgn/sbin/bnetd' -D -c '/home/espionage724/run-pvpgn/etc/pvpgn/bnetd-real.conf'
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Execute
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Information
%%%%%%%%%%%%%%%%%%%%

- Starts bnetd in dedicated screen session and changes Terminal over to the screen session
- To leave screen session without closing program; hit Ctrl + A and D (hold Ctrl and then press A and D at the same time)

%%%%%%%%%%%%%%%%%%%%
%%%%% bnetd
%%%%%%%%%%%%%%%%%%%%

'/home/espionage724/pvpgn.sh' && screen -r pvpgn

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Accounts
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Grant Administrator
%%%%% Do this after creating account in-game and either make sure you're the first user, or change the uid below
%%%%%%%%%%%%%%%%%%%%

mysql -u root -p

update `pvpgn`.`pvpgn_BNET` set `auth_admin` = 'true' , `auth_command_groups` = '255' where `uid` = '1'
FLUSH TABLES;
EXIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% systemd Startup Scripts
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Notes
%%%%%%%%%%%%%%%%%%%%

- Autostarts bnetd via script created above (and thus, in its own screen session)
- Use pvpgn for addressing service name for use with status and service management

%%%%%%%%%%%%%%%%%%%%
%%%%% bnetd
%%%%%%%%%%%%%%%%%%%%

sudo nano '/usr/lib/systemd/system/pvpgn.service'

-------------------------
[Unit]
Description=PvPGN PRO
After=network.target

[Service]
Type=forking
User=espionage724
Group=espionage724
WorkingDirectory=/home/espionage724/run/bin
ExecStart=/bin/bash /home/espionage724/pvpgn.sh start
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Enable Services
%%%%%%%%%%%%%%%%%%%%

sudo systemctl daemon-reload && sudo systemctl enable pvpgn

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################