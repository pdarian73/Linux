%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- TrinityCore (6.x) setup for Realm of Espionage
- Commands below are for Fedora 23 Server
- Assumes presense of three different machines, one for TrinityCore, one for MySQL and/or webserver for SOAP, and one to run WoW on (desktop/client)
- Do commands below on TrinityCore computer unless otherwise specified
- Any accounts using Two-factor authentication must not have their token_key contain any numbers above 7 (no 8 or 9) and can not be more than 16 characters long
- TODO: Update this when/if applicable to TC-WotLK standards (see Servers/TrinityCore-WotLK.txt)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Firewall Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% bnetserver
%%%%%%%%%%%%%%%%%%%%

sudo firewall-cmd --permanent --add-port=1118/tcp
sudo firewall-cmd --permanent --add-port=1119/tcp

%%%%%%%%%%%%%%%%%%%%
%%%%% worldserver
%%%%%%%%%%%%%%%%%%%%

sudo firewall-cmd --permanent --add-port=8085/tcp
sudo firewall-cmd --permanent --add-port=8086/tcp

%%%%%%%%%%%%%%%%%%%%
%%%%% SOAP
%%%%%%%%%%%%%%%%%%%%

sudo firewall-cmd --permanent --add-port=7879/tcp

%%%%%%%%%%%%%%%%%%%%
%%%%% distcc
%%%%%%%%%%%%%%%%%%%%

sudo firewall-cmd --permanent --add-port=3632/tcp

%%%%%%%%%%%%%%%%%%%%
%%%%% Reload
%%%%%%%%%%%%%%%%%%%%

sudo firewall-cmd --reload

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo dnf install screen distcc wget p7zip autoconf libtool gcc gcc-c++ make cmake git ncurses-devel openssl openssl-devel mariadb mariadb-devel readline-devel zlib-devel bzip2-devel boost-devel zeromq3-devel && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Git Management
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Download
%%%%%%%%%%%%%%%%%%%%

cd '/home/espionage724' && git clone -b 6.x https://github.com/TrinityCore/TrinityCore.git trinitycore && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Update
%%%%%%%%%%%%%%%%%%%%

cd '/home/espionage724/trinitycore' && git pull origin 6.x && cd '/home/espionage724' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configure MariaDB Client
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Tell TrinityCore where MySQL server is
%%%%%%%%%%%%%%%%%%%%

sudo nano '/etc/my.cnf.d/client.cnf'

-------------------------
[client]
host = 192.168.1.154
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Compile and Install TrinityCore
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Compile
%%%%%%%%%%%%%%%%%%%%

mkdir '/home/espionage724/build' '/home/espionage724/run' && cd '/home/espionage724/build'
CC='distcc gcc' CXX='distcc g++' cmake '/home/espionage724/trinitycore' -DCMAKE_INSTALL_PREFIX='/home/espionage724/run' -DTOOLS=1 -DWITH_WARNINGS=0 -DWITH_COREDEBUG=0 -DUSE_COREPCH=0 -DUSE_SCRIPTPCH=0 -DCMAKE_CXX_FLAGS='-O2 -pipe -march=amdfam10' -DCMAKE_C_FLAGS='-O2 -pipe -march=amdfam10' && sync
DISTCC_HOSTS='192.168.1.150/8 localhost/2' make -j10 install && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configure TrinityCore
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Copy Configuration Files
%%%%% Only do for initial setup
%%%%%%%%%%%%%%%%%%%%

cp '/home/espionage724/run/etc/bnetserver.conf.dist' '/home/espionage724/run/etc/bnetserver.conf'
cp '/home/espionage724/run/etc/worldserver.conf.dist' '/home/espionage724/run/etc/worldserver.conf'

%%%%%%%%%%%%%%%%%%%%
%%%%% bnetserver Configuration
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/run/etc/bnetserver.conf'

-------------------------
WrongPass.MaxCount = 5

LoginDatabaseInfo = "192.168.1.154;3306;auth;x;auth"

Updates.EnableDatabases = 1
Updates.CleanDeadRefMaxCount = -1
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% worldserver Configuration
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/run/etc/worldserver.conf'

-------------------------
LoginDatabaseInfo     = "192.168.1.154;3306;auth;x;auth"
WorldDatabaseInfo     = "192.168.1.154;3306;world;x;world"
CharacterDatabaseInfo = "192.168.1.154;3306;characters;x;characters"
HotfixDatabaseInfo    = "192.168.1.154;3306;hotfixes;x;hotfixes"

Compression = 9
PlayerSave.Stats.MinLevel = 10
mmap.enablePathFinding = 1
TargetPosRecalculateRange = 0.5
MaxCoreStuckTime = 10
CleanCharacterDB = 1
PersistentCharacterCleanFlags = 14

RealmZone = 28
Motd = "Welcome to the Realm of Espionage Warlords of Draenor server!"
Server.LoginInfo = 1
FeatureSystem.BpayStore.Enabled = 1
FeatureSystem.CharacterUndelete.Enabled = 1

Updates.CleanDeadRefMaxCount = -1

Warden.Enabled = 1
Warden.ClientCheckFailAction = 2

ChatFakeMessagePreventing = 1
ChatStrictLinkChecking.Severity = 2
PreserveCustomChannels = 0
PreserveCustomChannelDuration = 7

Support.TicketsEnabled = 1
Support.BugsEnabled = 1
Support.ComplaintsEnabled = 1
Support.SuggestionsEnabled = 1

Wintergrasp.Enable = 1

SOAP.Enabled = 1
SOAP.IP = "0.0.0.0"
SOAP.Port = 7879

CharDelete.KeepDays = 7

PacketSpoof.Policy = 2
PacketSpoof.BanMode = 2
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Content Extractors/Patcher Management
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Copy extractors and assemblers from TrinityCore computer to Client computer
%%%%% Run on Client
%%%%%%%%%%%%%%%%%%%%

scp espionage724@192.168.1.152:/home/espionage724/run/bin/mapextractor '/home/espionage724/Downloads'
scp espionage724@192.168.1.152:/home/espionage724/run/bin/vmap4extractor '/home/espionage724/Downloads'
scp espionage724@192.168.1.152:/home/espionage724/run/bin/vmap4assembler '/home/espionage724/Downloads'
scp espionage724@192.168.1.152:/home/espionage724/run/bin/mmaps_generator '/home/espionage724/Downloads'
scp espionage724@192.168.1.152:/home/espionage724/run/bin/connection_patcher '/home/espionage724/Downloads'

%%%%%%%%%%%%%%%%%%%%
%%%%% Extract and Build Content
%%%%%
%%%%% Run on client computer
%%%%% mmaps_generator requires boost libraries present
%%%%% 
%%%%% Use /? flag to show available flags for mapextractor and vmap4extractor
%%%%% mmaps_generator flags: https://github.com/TrinityCore/TrinityCore/tree/6.x/src/tools/mmaps_generator/Info
%%%%% mmaps_generator took 1.2 hours with WoD
%%%%% 
%%%%% WoD (20574): dbc = 207.9MB, maps = 1.1GB, vmaps = 3.2GB, mmaps = 3.5GB
%%%%%%%%%%%%%%%%%%%%

sudo dnf install boost

cd '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/Program Files (x86)/World of Warcraft'
mkdir '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/Program Files (x86)/World of Warcraft/vmaps' '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/Program Files (x86)/World of Warcraft/mmaps'

'/home/espionage724/Downloads/mapextractor' -f 0 && sync
'/home/espionage724/Downloads/vmap4extractor' -l && sync
'/home/espionage724/Downloads/vmap4assembler' Buildings vmaps && sync
'/home/espionage724/Downloads/mmaps_generator' --bigBaseUnit true --threads 8 && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% connection_patcher
%%%%% 
%%%%% To be done for 6.x on computers with WoW client to allow connections to private servers
%%%%% Use -h or /? flag to show available flags
%%%%% Use of -m implys WoW is installed to a separate Wine prefix somewhere other than the default ~/.wine location (in other words, if it's installed to that default prefix, you don't need that flag)
%%%%% Start WoW at least once from both Battle.net launcher and standalone exe (may not be needed)
%%%%% Run connection_patcher while WoW is running and you're logged into a character on Retail
%%%%%%%%%%%%%%%%%%%%

'/home/espionage724/Downloads/connection_patcher' -m '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/users/Public/Application Data' '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/Program Files (x86)/World of Warcraft/Wow-64.exe'

'/home/espionage724/Downloads/connection_patcher' -m '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/users/Public/Application Data' '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/Program Files (x86)/World of Warcraft/Wow.exe'

%%%%%%%%%%%%%%%%%%%%
%%%%% Send Content to Server
%%%%%%%%%%%%%%%%%%%%

scp -r '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/Program Files (x86)/World of Warcraft/dbc' espionage724@192.168.1.152:/home/espionage724/run/bin
scp -r '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/Program Files (x86)/World of Warcraft/maps' espionage724@192.168.1.152:/home/espionage724/run/bin
scp -r '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/Program Files (x86)/World of Warcraft/vmaps' espionage724@192.168.1.152:/home/espionage724/run/bin
scp -r '/home/espionage724/Wine Prefixes/World of Warcraft/drive_c/Program Files (x86)/World of Warcraft/mmaps' espionage724@192.168.1.152:/home/espionage724/run/bin

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Database Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Create Databases, users, and grant permissions
%%%%% Do on MySQL computer
%%%%%%%%%%%%%%%%%%%%

mysql -u root -p

CREATE USER auth;
SET PASSWORD FOR 'auth' = PASSWORD ('x');
CREATE DATABASE auth;
GRANT ALL PRIVILEGES ON auth.* to 'auth'@'192.168.1.152' IDENTIFIED BY 'x';

CREATE USER characters;
SET PASSWORD FOR 'characters' = PASSWORD ('x');
CREATE DATABASE characters;
GRANT ALL PRIVILEGES ON characters.* to 'characters'@'192.168.1.152' IDENTIFIED BY 'x';

CREATE USER world;
SET PASSWORD FOR 'world' = PASSWORD ('x');
CREATE DATABASE world;
GRANT ALL PRIVILEGES ON world.* to 'world'@'192.168.1.152' IDENTIFIED BY 'x';

CREATE USER hotfixes;
SET PASSWORD FOR 'hotfixes' = PASSWORD ('x');
CREATE DATABASE hotfixes;
GRANT ALL PRIVILEGES ON hotfixes.* to 'hotfixes'@'192.168.1.152' IDENTIFIED BY 'x';

FLUSH PRIVILEGES;
EXIT

%%%%%%%%%%%%%%%%%%%%
%%%%% World Database Prepare
%%%%% URL and filenames need updated when new world database releases (currently on TDB 6.03)
%%%%%%%%%%%%%%%%%%%%

cd '/home/espionage724/run/bin'
wget https://github.com/TrinityCore/TrinityCore/releases/download/TDB6.03/TDB_full_6.03_2015_11_08.7z
7za x '/home/espionage724/run/bin/TDB_full_6.03_2015_11_08.7z' 'TDB_full_world_6.03_2015_11_08.sql' 'TDB_full_hotfixes_6.03_2015_11_08.sql'
rm '/home/espionage724/run/bin/TDB_full_6.03_2015_11_08.7z'

%%%%%%%%%%%%%%%%%%%%
%%%%% realmlist
%%%%% Do this after starting bnetserver once and on MySQL computer
%%%%%%%%%%%%%%%%%%%%

mysql -u root -p

update `auth`.`realmlist` set `name` = 'RoE (WoD)' , `address` = 'realmofespionage.com' where `id` = '1';
FLUSH TABLES;
EXIT

%%%%%%%%%%%%%%%%%%%%
%%%%% SOAP Database
%%%%% Do this after starting bnetserver once and on MySQL computer
%%%%%%%%%%%%%%%%%%%%

mysql -u root -p

CREATE USER soap;
SET PASSWORD FOR 'soap' = PASSWORD ('x');
GRANT SELECT, UPDATE on auth.account to 'soap'@'%' IDENTIFIED BY 'x';
FLUSH PRIVILEGES;
EXIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Script Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% bnetserver
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/bnet.sh' && chmod +x '/home/espionage724/bnet.sh'

-------------------------
cd '/home/espionage724/run/bin'
screen -dmS bnet '/home/espionage724/run/bin/bnetserver'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% worldserver
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/world.sh' && chmod +x '/home/espionage724/world.sh'

-------------------------
cd '/home/espionage724/run/bin'
screen -dmS world '/home/espionage724/run/bin/worldserver'
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Execute
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Information
%%%%%%%%%%%%%%%%%%%%

- Starts authserver and worldserver in dedicated screen sessions and changes Terminal over to the screen session
- To leave screen session without closing program; hit Ctrl + A and D (hold Ctrl and then press A and D at the same time)

%%%%%%%%%%%%%%%%%%%%
%%%%% bnetserver
%%%%%%%%%%%%%%%%%%%%

'/home/espionage724/bnet.sh' && screen -r bnet

%%%%%%%%%%%%%%%%%%%%
%%%%% worldserver
%%%%%%%%%%%%%%%%%%%%

'/home/espionage724/world.sh' && screen -r world

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Accounts
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Main Administrator
%%%%% Do from worldserver console
%%%%%%%%%%%%%%%%%%%%

bnetaccount create Espionage724@realmofespionage x
account set gmlevel 1#1 3 -1

%%%%%%%%%%%%%%%%%%%%
%%%%% SOAP
%%%%% Do from worldserver console
%%%%%%%%%%%%%%%%%%%%

account create soap x
account set gmlevel soap 3 -1
rbac account grant soap 219 -1
rbac account grant soap 228 -1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% systemd Startup Scripts
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Notes
%%%%%%%%%%%%%%%%%%%%

- Autostarts bnetserver and worldserver via scripts created above (and thus, in their own screen session)
- Use bnet and world for addressing service names for use with status and service management

%%%%%%%%%%%%%%%%%%%%
%%%%% bnetserver
%%%%%%%%%%%%%%%%%%%%

sudo nano '/usr/lib/systemd/system/bnet.service'

-------------------------
[Unit]
Description=TrinityCore bnetserver
After=network.target

[Service]
Type=forking
User=espionage724
Group=espionage724
WorkingDirectory=/home/espionage724/run/bin
ExecStart=/bin/bash /home/espionage724/bnet.sh start
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% worldserver
%%%%%%%%%%%%%%%%%%%%

sudo nano '/usr/lib/systemd/system/world.service'

-------------------------
[Unit]
Description=TrinityCore worldserver
After=network.target

[Service]
Type=forking
User=espionage724
Group=espionage724
WorkingDirectory=/home/espionage724/run/bin
ExecStart=/bin/bash /home/espionage724/world.sh start
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Enable Services
%%%%%%%%%%%%%%%%%%%%

sudo systemctl daemon-reload && sudo systemctl enable bnet world

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################